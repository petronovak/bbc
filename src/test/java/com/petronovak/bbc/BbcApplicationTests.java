package com.petronovak.bbc;

import static org.mockito.Mockito.times;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.petronovak.bbc.service.CacheService;
import com.petronovak.bbc.service.HttpService;
import com.petronovak.bbc.service.impl.CategoryServiceImpl;


@RunWith(SpringRunner.class)
@SpringBootTest
public class BbcApplicationTests {
	
	private static final String CAT_SPORT				= "sport";
	private static final String CAT_NEWS				= "news";
	private static final String CAT_TV					= "tv";
	private static final String SUB_CAT_FOOTBALL		= "football";
	private static final String SUB_CAT_SOMEONE			= "someone";
	private static final String SUB_CAT_MOVIES			= "movies";
	private static final String PAGE_1					= "<html>Page 1</html>";
	private static final String PAGE_2					= "<html>Page 2</html>";
	private static final String PAGE_3					= "<html>Page 3</html>";
	
	private static final String ID_1 					= "1";
	private static final String ID_2 					= "2";
	private static final String ID_3 					= "3";

	@Test
	public void contextLoads() {
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void emptyCacheTest() {
		CategoryServiceImpl service = new CategoryServiceImpl();
		CacheService cacheService = createCacheService();
		HttpService httpService = createHttpService();
		
		service.setCacheService(cacheService);
		service.setHttpService(httpService);
		
		String page = service.getPage(CAT_SPORT, SUB_CAT_FOOTBALL, ID_1);
		
		//Checking results
		Assert.assertEquals(PAGE_1, page);
		Mockito.verify(cacheService, times(1)).get(CategoryServiceImpl.getPageKey(CAT_SPORT, SUB_CAT_FOOTBALL, ID_1));
		Mockito.verify(cacheService, times(1)).put(CategoryServiceImpl.getPageKey(CAT_SPORT, SUB_CAT_FOOTBALL, ID_1), PAGE_1);
		Mockito.verify(httpService, times(1)).getPageString(CAT_SPORT, SUB_CAT_FOOTBALL, ID_1);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void fullCacheTest() {
		CategoryServiceImpl service = new CategoryServiceImpl();
		CacheService cacheService = createCacheService();
		HttpService httpService = createHttpService();
		
		service.setCacheService(cacheService);
		service.setHttpService(httpService);
		
		String page = service.getPage(CAT_NEWS, SUB_CAT_SOMEONE, ID_2);

		//Checking results
		Assert.assertEquals(PAGE_2, page);
		Mockito.verify(cacheService, times(1)).get(CategoryServiceImpl.getPageKey(CAT_NEWS, SUB_CAT_SOMEONE, ID_2));
		Mockito.verify(cacheService, times(0)).put(CategoryServiceImpl.getPageKey(CAT_NEWS, SUB_CAT_SOMEONE, ID_2), PAGE_2);
		Mockito.verify(httpService, times(0)).getPageString(CAT_NEWS, SUB_CAT_SOMEONE, ID_2);
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void doubleRequestsTest() {
		CategoryServiceImpl service = new CategoryServiceImpl();
		CacheService cacheService = createCacheService();
		HttpService httpService = createHttpService();
		
		service.setCacheService(cacheService);
		service.setHttpService(httpService);

		//First request
		String page = service.getPage(CAT_TV, SUB_CAT_MOVIES, ID_3);

		//Checking results
		Assert.assertEquals(PAGE_3, page);
		Mockito.verify(cacheService, times(1)).get(CategoryServiceImpl.getPageKey(CAT_TV, SUB_CAT_MOVIES, ID_3));
		Mockito.verify(cacheService, times(1)).put(CategoryServiceImpl.getPageKey(CAT_TV, SUB_CAT_MOVIES, ID_3), PAGE_3);
		Mockito.verify(httpService, times(1)).getPageString(CAT_TV, SUB_CAT_MOVIES, ID_3);
		
		//Second request
		page = service.getPage(CAT_TV, SUB_CAT_MOVIES, ID_3);

		//Checking results
		Assert.assertEquals(PAGE_3, page);
		Mockito.verify(cacheService, times(2)).get(CategoryServiceImpl.getPageKey(CAT_TV, SUB_CAT_MOVIES, ID_3));
		Mockito.verify(cacheService, times(1)).put(CategoryServiceImpl.getPageKey(CAT_TV, SUB_CAT_MOVIES, ID_3), PAGE_3);
		Mockito.verify(httpService, times(1)).getPageString(CAT_TV, SUB_CAT_MOVIES, ID_3);
	}
	
	private CacheService createCacheService() {
		CacheService service = Mockito.mock(CacheService.class);
		Mockito.when(service.get(CategoryServiceImpl.getPageKey(CAT_SPORT, SUB_CAT_FOOTBALL, ID_1))).thenReturn(null);
		Mockito.when(service.get(CategoryServiceImpl.getPageKey(CAT_NEWS, SUB_CAT_SOMEONE, ID_2))).thenReturn(PAGE_2);
		Mockito.when(service.get(CategoryServiceImpl.getPageKey(CAT_TV, SUB_CAT_MOVIES, ID_3))).thenReturn(null, PAGE_3);
		
		return service;
	}

	private HttpService createHttpService() {
		HttpService service = Mockito.mock(HttpService.class);
		Mockito.when(service.getPageString(CAT_SPORT, SUB_CAT_FOOTBALL, ID_1)).thenReturn(PAGE_1);
		Mockito.when(service.getPageString(CAT_TV, SUB_CAT_MOVIES, ID_3)).thenReturn(PAGE_3);
		
		return service;
	}

}
