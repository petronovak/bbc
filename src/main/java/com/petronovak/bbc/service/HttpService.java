package com.petronovak.bbc.service;

public interface HttpService {

	public String getPageString(String category, String subCategory, String id);
}
