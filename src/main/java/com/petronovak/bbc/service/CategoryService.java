package com.petronovak.bbc.service;

public interface CategoryService {

	public String getPage(String category, String subCategory, String id);
}
