package com.petronovak.bbc.service;

public interface CacheService {

	public String get(String key);
	
	public void put(String key, String value);
}
