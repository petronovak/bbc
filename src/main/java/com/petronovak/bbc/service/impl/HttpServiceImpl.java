package com.petronovak.bbc.service.impl;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

import javax.annotation.PostConstruct;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Service;

import com.petronovak.bbc.service.HttpService;

@Service
public class HttpServiceImpl implements HttpService {
	
	private static final String SERVER_URL	= "http://www.bbc.com";
	
	private CloseableHttpClient httpclient;

	@PostConstruct
	public void init() {
		httpclient = HttpClients.createDefault();
		System.out.println("HTTP service initialized");
	}

	@Override
	public String getPageString(String category, String subCategory, String id) {
		HttpGet httpGet = new HttpGet(SERVER_URL + "/" + category + "/" + subCategory + "/" + id);
		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(httpGet);
		    HttpEntity entity = response.getEntity();
		    ByteArrayOutputStream bout = new ByteArrayOutputStream();
		    IOUtils.copy(entity.getContent(), bout);
		    
		    EntityUtils.consume(entity);
		    return bout.toString(StandardCharsets.UTF_8.name());
		} catch (Exception e) {
			return null;
		} finally {
			try { response.close(); } catch (Exception e) {}
		}
	}

}
