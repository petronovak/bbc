package com.petronovak.bbc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.petronovak.bbc.service.CacheService;
import com.petronovak.bbc.service.CategoryService;
import com.petronovak.bbc.service.HttpService;

@Service
public class CategoryServiceImpl implements CategoryService {
	
	private static final String SEP			= ";";

	@Autowired
	private CacheService cacheService;

	@Autowired
	private HttpService httpService;

	@Override
	public String getPage(String category, String subCategory, String id) {
		String key = getPageKey(category, subCategory, id);
		String page = cacheService.get(key);
		if (!isEmpty(page)) {
			return page;
		}
		
		page = httpService.getPageString(category, subCategory, id);
		if (!isEmpty(page)) {
			cacheService.put(key, page);
		}
		
		return page;
	}
	
	/**
	 * For JUnit-test only
	 * 
	 * @param cacheService
	 */
	@Deprecated
	public void setCacheService(CacheService cacheService) {
		this.cacheService = cacheService;
	}

	/**
	 * For JUnit-test only
	 * 
	 * @param httpService
	 */
	@Deprecated
	public void setHttpService(HttpService httpService) {
		this.httpService = httpService;
	}

	public static String getPageKey(String category, String subCategory, String id) {
		return "bbc" + SEP + category + SEP + subCategory + SEP + id;
	}
	
	private static boolean isEmpty(String str) {
		return (str == null || str.isEmpty());
	}
}
