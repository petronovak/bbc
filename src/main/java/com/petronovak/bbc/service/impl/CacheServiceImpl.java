package com.petronovak.bbc.service.impl;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.redisson.Redisson;
import org.redisson.api.RMapCache;
import org.redisson.config.Config;
import org.springframework.stereotype.Service;

import com.petronovak.bbc.service.CacheService;

@Service
public class CacheServiceImpl implements CacheService {

	private static final String REDIS_URL	= "http://localhost:6379";
	private static final int CACHE_TIME		= 10;

	private RMapCache<String, String> cache;
	
	@PostConstruct
	public void init() {
		try {
	        Config config = new Config();
	        config.useSingleServer().setAddress(REDIS_URL);
	        cache = Redisson.create(config).getMapCache("bbcCache");
	        System.out.println("Cache service initialized with URL=" + REDIS_URL);
		} catch (Exception e) {
			cache = null;
			System.out.println("Error initialize cache service with URL=" + REDIS_URL);
		}
	}

	@Override
	public String get(String key) {
		return cache.get(key);
	}

	@Override
	public void put(String key, String value) {
		cache.fastPut(key, value, CACHE_TIME, TimeUnit.MINUTES);
	}
}
