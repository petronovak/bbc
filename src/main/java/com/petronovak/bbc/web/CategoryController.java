package com.petronovak.bbc.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.petronovak.bbc.service.CategoryService;


@Controller
public class CategoryController {
	
	private static final String ERROR_PAGE = "<html>Server error</html>";

	@Autowired
	private CategoryService categoryService;

	@RequestMapping(value = "{category}/{subCategory}/{id}", method = RequestMethod.GET)
	public @ResponseBody String get(@PathVariable("category") String category,
			@PathVariable("subCategory") String subCategory, @PathVariable("id") String id) {
		
		try {
			String page = categoryService.getPage(category, subCategory, id);
			if (page != null) return page;
		} catch (Exception e) {
		}
		
		return ERROR_PAGE;
	}
}
